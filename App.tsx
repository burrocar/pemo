import React, {Component, ReactElement} from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {Button} from "react-native-elements";
import {createAppContainer} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";
import {IProps} from "./models/iProps";
import {State} from "./models/state";
import {QuizComponent} from "./Quiz";

class AppComponent extends Component {
    public static navigationOptions = {
        title: "PeMo - Pedagogue's Motivation",
    };
    public props: IProps;
    public state: State = new State({currentQuoteIndex: -1, quoteList: []});

    constructor(props: IProps) {
        super(props);
        this.getNewQuote = this.getNewQuote.bind(this);
        new Promise<string[][]>((resolve) => {
            const quotes: string[][] = require("./assets/quotes.json");
            resolve(quotes);
        }).then((quotes: string[][]) => {
            quotes.forEach((quote: string[]) => {
                this.state.quoteList.push(quote);
            });
        });
    }

    public getNewQuote() {
        this.setState(
            new State({
                currentQuoteIndex: Math.floor(
                    Math.random() * this.state.quoteList.length,
                ),
            }),
        );
    }

    public render(): React.ReactElement {
        let quoteText: ReactElement = <Text />;
        let quoteAuthor: ReactElement = <Text />;
        if (this.state.currentQuoteIndex > 0) {
            quoteText = (
                <Text>
                    {this.state.quoteList[this.state.currentQuoteIndex][1]}
                </Text>
            );
            quoteAuthor = (
                <Text style={styles.marginTop}>
                    {this.state.quoteList[this.state.currentQuoteIndex][0]}
                </Text>
            );
        }
        return (
            <View style={styles.container}>
                <Button
                    title='Quiz'
                    buttonStyle={styles.marginTop}
                    onPress={() => this.props.navigation.navigate("Quiz")}
                />
                <Button
                    title="Inspirier' mich!"
                    buttonStyle={styles.marginTop}
                    onPress={this.getNewQuote}
                />
                <ScrollView style={styles.scrollView}>
                    {quoteText}
                    {quoteAuthor}
                </ScrollView>
            </View>
        );
    }
}

const MainNavigator = createStackNavigator({
    App: {screen: AppComponent},
    Quiz: {screen: QuizComponent},
});
const App = createAppContainer(MainNavigator);
export default App;
const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "#fff",
        justifyContent: "center",
    },
    marginTop: {
        marginTop: 10,
    },
    scrollView: {
        padding: 20,
    },
});
