export class TipsAndFeedback {
    public chosenFeedback: string;
    public notChosenFeedback: string;
    public tip: string;

    constructor(init?: Partial<TipsAndFeedback>) {
        Object.assign(this, init);
    }
}
