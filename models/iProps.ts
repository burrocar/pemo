import {NavigationStackProp} from "react-navigation-stack";

export interface IProps {
    navigation: NavigationStackProp<{ userId: string }>;
}
