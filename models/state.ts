export class State {
    public currentLevel: number;
    public currentQuoteIndex: number;
    public isFrozen: boolean;
    public overallScoreMax: number;
    public overallScoreRaw: number;
    public quoteList: string[][];

    constructor(init?: Partial<State>) {
        Object.assign(this, init);
    }
}
