#!/usr/bin/env bash

\cp h5p.html android/app/src/main/assets/
rm -rf android/app/src/main/assets/dist
rm -rf android/app/src/main/assets/h5p
\cp -r assets/dist android/app/src/main/assets/dist
\cp -r assets/h5p android/app/src/main/assets/h5p
react-native run-android
