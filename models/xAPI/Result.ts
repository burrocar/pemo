import Extensions from './Extensions';
import Score from "src/app/models/xAPI/Score";

interface Result {
  duration?: string;
  extensions?: Extensions;
  response?: string;
  score?: Score;
}

export default Result;
