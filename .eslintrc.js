module.exports = {
    root: true,
    extends: '@react-native-community',
    rules: {
        "arrow-parens": "error",
        "jsx-quotes": "off",
        quotes: "off",
    }
};
