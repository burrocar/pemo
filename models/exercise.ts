import {Answer} from './answer';

export class Exercise {
    public answers: Answer[];
    public behaviour: object;
    public confirmCheck: object;
    public confirmRetry: object;
    public overallFeedback: object;
    public question: string;
    public UI: object;

    constructor(init?: Partial<Exercise>) {
        Object.assign(this, init);
    }
}
