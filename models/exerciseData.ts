export class ExerciseData {
    public distractors: string[];
    public level: number;
    public source: string;
    public target: string;

    constructor(init?: Partial<ExerciseData>) {
        Object.assign(this, init);
    }
}
