interface Score {
    max: number;
    min: number;
    raw: number;
    scaled: number;
}

export default Score;
