Building (in general, esp. for debugging):
1. Create `index.html` in `android/app/src/main/assets`.
2. Copy `assets/dist` and `assets/h5p` to `android/app/src/main/assets`.
3. Configure Gradle in gradle.properties:
`MYAPP_UPLOAD_STORE_FILE=my-release-key.keystore`
`MYAPP_UPLOAD_KEY_ALIAS=alias_name`
`MYAPP_UPLOAD_STORE_PASSWORD=keystore_password`
`MYAPP_UPLOAD_KEY_PASSWORD=key_password`
4. Execute the bash script using `./build_android.sh` to build the application, OR use the "android" script in `package.json`, e.g. `npm run android`.

Building an APK file for releasing the app:
1. `cd android`
2. `rm -rf app/src/main/res/raw`
3. `./gradlew assembleRelease`
4. The resulting APK file is located at `android/app/build/outputs/apk/release/app-release.apk`
