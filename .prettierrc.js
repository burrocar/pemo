module.exports = {
  arrowParens: "always",
  bracketSpacing: false,
  jsxBracketSameLine: true,
  jsxSingleQuote: true,
  requireParens: true,
  singleQuote: false,
  tabWidth: 4,
  trailingComma: 'all',
};
