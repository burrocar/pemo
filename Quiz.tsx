// TODO: increase the initial H5P timeout
import React, {Component, ReactElement} from "react";
import {
    Dimensions,
    NativeSyntheticEvent,
    ScrollView,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
} from "react-native";
import {Input} from "react-native-elements";
import WebView from "react-native-webview";
import {WebViewMessage} from "react-native-webview/lib/WebViewTypes";
import H5Pjs from "./h5pJS";
import {Answer} from "./models/answer";
import {Exercise} from "./models/exercise";
import {ExerciseData} from "./models/exerciseData";
import {IProps} from "./models/iProps";
import {State} from "./models/state";
import {TipsAndFeedback} from "./models/tipsAndFeedback";
import Result from "./models/xAPI/Result";
import {XAPIevent} from "./models/xAPIevent";
import AsyncStorage from "@react-native-community/async-storage";

export class QuizComponent extends Component {
    public static navigationOptions = {
        title: "Quiz",
    };
    public androidBaseUrl = "file:///android_asset";
    public exerciseDataList: ExerciseData[];
    public levelInput: Input;
    public maxLevel: number;
    public multiChoiceTemplate: object;
    public previousTarget: string;
    public props: IProps;
    public state: State = new State({
        overallScoreMax: 0,
        overallScoreRaw: 0,
        isFrozen: false,
        currentLevel: 1,
    });
    public storageKeyQuizState = "pemo/quiz/state";
    public webView: WebView;

    constructor(props: IProps) {
        super(props);
        AsyncStorage.getItem(this.storageKeyQuizState).then(
            (result: string | null) => {
                this.setState(JSON.parse(result as string));
            },
        );
        new Promise<ExerciseData[]>((resolve) => {
            const edl: ExerciseData[] = require("./assets/exercises.json");
            resolve(edl);
        }).then((edl: ExerciseData[]) => {
            this.exerciseDataList = edl;
            this.maxLevel = Math.max(
                ...this.exerciseDataList.map((ed: ExerciseData) => ed.level),
            );
            new Promise<object>((resolve) => {
                const template: object = require("./assets/h5p/multi_choice/content/1.json");
                resolve(template);
            }).then((template: object) => {
                this.multiChoiceTemplate = template;
                // dirty hack to get H5P working
                setTimeout(() => {
                    this.showNextExercise();
                }, 1500);
            });
        });
    }

    public changeCurrentLevel(levelString: string) {
        const newLevel: number = +levelString;
        if (newLevel && !isNaN(newLevel)) {
            const newState: State = {...this.state};
            newState.currentLevel = newLevel;
            this.setState(newState, this.showNextExercise);
        }
    }

    public finishExercise(h5pEvent: XAPIevent) {
        this.webView.injectJavaScript(`
                        IFRAME = document.querySelector('.h5p-iframe');
                        SOLUTIONS_BUTTON = IFRAME.contentDocument.querySelector('.h5p-question-show-solution');
                        if (SOLUTIONS_BUTTON) {
                            SOLUTIONS_BUTTON.click();
                        }
                    `);
        const result: Result = h5pEvent.data.statement.result as Result;
        const raw: number = this.state.overallScoreRaw + result.score.raw;
        const max: number = this.state.overallScoreMax + result.score.max;
        this.setState(
            new State({
                overallScoreRaw: raw,
                overallScoreMax: max,
                isFrozen: true,
            }),
            this.saveState,
        );
    }

    public getNewExerciseIndex(exerciseData: ExerciseData[]): number {
        let idx: number = Math.floor(Math.random() * exerciseData.length);
        while (exerciseData[idx].target === this.previousTarget) {
            idx = this.getNewExerciseIndex(exerciseData);
        }
        return idx;
    }

    public initH5P() {
        this.webView.injectJavaScript(H5Pjs);
        this.webView.injectJavaScript(`
                            H5P.externalDispatcher.on('xAPI', (event) => {
                            // results are only available when a task has been completed/answered, not in the "attempted" or "interacted" stages
                            if (event.data.statement.verb.id === 'http://adlnet.gov/expapi/verbs/answered' && event.data.statement.result) {
                                window.ReactNativeWebView.postMessage(JSON.stringify(event));
                            }
                        });
                        `);
    }

    public makeExercise(template: any): Exercise {
        const exerciseDataByLevel: ExerciseData[] = this.exerciseDataList.filter(
            (ed: ExerciseData) => ed.level === this.state.currentLevel,
        );
        const idx: number = this.getNewExerciseIndex(exerciseDataByLevel);
        const exerciseData: ExerciseData = exerciseDataByLevel[idx];
        const exercise: Exercise = new Exercise({
            UI: template.UI,
            answers: [
                new Answer({
                    correct: true,
                    text: exerciseData.target,
                    tipsAndFeedback: new TipsAndFeedback(),
                }),
            ],
            behaviour: template.behaviour,
            confirmCheck: template.confirmCheck,
            confirmRetry: template.confirmRetry,
            overallFeedback: template.overallFeedback,
            question: `Welches Wort passt am wenigsten zu '${
                exerciseData.source
            }'?`,
        });
        exerciseData.distractors.forEach((distractor) =>
            exercise.answers.push(
                new Answer({
                    correct: false,
                    text: distractor,
                    tipsAndFeedback: new TipsAndFeedback(),
                }),
            ),
        );
        this.previousTarget = exerciseData.target;
        return exercise;
    }

    public render(): ReactElement {
        return (
            <ScrollView style={styles.viewColumn}>
                <View style={styles.viewRow}>
                    <Text style={styles.score}>
                        Punkte: {this.state.overallScoreRaw}/
                        {this.state.overallScoreMax}
                    </Text>
                    <Input
                        containerStyle={styles.levelInput}
                        keyboardType={"numeric"}
                        label={"Level"}
                        maxLength={2}
                        onChangeText={this.changeCurrentLevel.bind(this)}
                        placeholder={"1"}
                        ref={(levelInput: Input) =>
                            (this.levelInput = levelInput)
                        }
                    />
                </View>
                <WebView
                    allowFileAccessFromFileURLs={true}
                    allowUniversalAccessFromFileURLs={true}
                    domStorageEnabled={true}
                    javaScriptEnabled={true}
                    mixedContentMode={"compatibility"}
                    onLoadEnd={() => this.initH5P()}
                    onMessage={(event: NativeSyntheticEvent<WebViewMessage>) =>
                        this.finishExercise(JSON.parse(
                            event.nativeEvent.data,
                        ) as XAPIevent)
                    }
                    originWhitelist={["*"]}
                    ref={(webView: WebView) => (this.webView = webView)}
                    source={{uri: "file:///android_asset/h5p.html"}}
                    style={styles.webView}
                />
                {this.renderOverlay()}
            </ScrollView>
        );
    }

    public renderOverlay(): ReactElement | null {
        return this.state.isFrozen ? (
            <View style={styles.overlay}>
                <TouchableHighlight
                    onPress={() => this.showNextExercise()}
                    style={styles.overlay}>
                    <Text />
                </TouchableHighlight>
            </View>
        ) : null;
    }

    public saveState(): void {
        AsyncStorage.setItem(
            this.storageKeyQuizState,
            JSON.stringify(this.state),
        ).then();
    }

    public showNextExercise() {
        const newState: State = {...this.state};
        newState.isFrozen = false;
        this.setState(newState);
        const exercise: Exercise = this.makeExercise(this.multiChoiceTemplate);
        const exerciseString: string = JSON.stringify(exercise);
        const frameJSpath: string =
            this.androidBaseUrl + "/dist/js/h5p-standalone-frame.min.js";
        this.webView.injectJavaScript(`
            H5Pjson = ${exerciseString};
            H5P.jQuery(".h5p-container")
            .empty()
            .h5p({
                frameCss: "${this.androidBaseUrl}/dist/styles/h5p.css",
                frameJs: "${frameJSpath}",
                h5pContent: "${this.androidBaseUrl}/h5p/" + "multi_choice",
            });
            `);
    }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    levelInput: {margin: 10, width: 100},
    overlay: {
        backgroundColor: "black",
        flex: 1,
        height: height,
        left: 0,
        opacity: 0,
        position: "absolute",
        top: 0,
        width: width,
    },
    score: {
        fontSize: 20,
        marginLeft: 10,
        marginTop: 30,
        width: 150,
    },
    viewColumn: {
        backgroundColor: "white",
        flex: 1,
        flexDirection: "column",
    },
    viewRow: {
        flex: 1,
        flexDirection: "row",
        maxHeight: 75,
    },
    webView: {
        minHeight: 300,
        marginTop: 20,
    },
});
