import {TipsAndFeedback} from './tipsAndFeedback';

export class Answer {
    public correct: boolean;
    public text: string;
    public tipsAndFeedback: TipsAndFeedback;

    constructor(init?: Partial<Answer>) {
        Object.assign(this, init);
    }
}
